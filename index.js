var express = require('express')
var app = express()

const HOST = '0.0.0.0';

app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))

app.get('/', function(request, response) {
  response.send('Hello World!')
})

app.listen(app.get('port'), HOST, function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
